package com.TugasCRUD.Controller;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.TugasCRUD.Model.Biodata;
import com.TugasCRUD.Repository.BiodataRepo;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiBiodata {
    @Autowired
    private BiodataRepo biodataRepo;

//    TES
    @GetMapping("/biodata")
    public ResponseEntity<List<Biodata>> GetAllBiodata() {
        try {
            List<Biodata> biodata = this.biodataRepo.findAll();
            return new ResponseEntity<>(biodata, HttpStatus.OK);
        } catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }
}
