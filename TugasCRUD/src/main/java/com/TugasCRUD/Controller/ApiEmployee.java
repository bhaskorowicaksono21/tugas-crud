package com.TugasCRUD.Controller;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.TugasCRUD.Model.Employee;
import com.TugasCRUD.Repository.EmployeeRepo;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiEmployee {
    @Autowired
    private EmployeeRepo employeeRepo;
	

    @GetMapping("/employeebycategory/{id}")
    public ResponseEntity<List<Employee>> GetAllVariantByCategoryId(@PathVariable("id") Long id)
    {
        try
        {
            List<Employee> employee = this.employeeRepo.FindByBiodataId(id);
            return new ResponseEntity<>(employee, HttpStatus.OK);
        }

        catch (Exception exception)
        {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
	}
        

    @GetMapping("/employee")
    public ResponseEntity<List<Employee>> GetAllEmployee() {
        try {
            List<Employee> employee = this.employeeRepo.findAll();
            return new ResponseEntity<>(employee, HttpStatus.OK);
        } catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/employee")
	public ResponseEntity<Object> SaveEmployee(@RequestBody Employee employee) {
		try {
			employee.setCreatedBy("Bhaskoro");
			employee.setCreatedAt(new Date());
			this.employeeRepo.save(employee);
			return new ResponseEntity<>(employee, HttpStatus.OK);
		} catch (Exception exception) {
			return new ResponseEntity<>("Failed", HttpStatus.BAD_REQUEST);
		}
	}

    @GetMapping("/employeemapped")
	public ResponseEntity<Map<String, Object>> GetAllPage(@RequestParam(defaultValue = "0") int page,
			@RequestParam(defaultValue = "5") int size) {
		try {
			List<Employee> employee = new ArrayList<>();
			Pageable pagingSort = PageRequest.of(page, size);

			Page<Employee> pageTuts;

			pageTuts = employeeRepo.findAll(pagingSort);

			employee = pageTuts.getContent();

			Map<String, Object> response = new HashMap<>();
			response.put("employee", employee);
			response.put("currentPage", pageTuts.getNumber());
			response.put("totalItems", pageTuts.getTotalElements());
			response.put("totalPages", pageTuts.getTotalPages());

			return new ResponseEntity<>(response, HttpStatus.OK);
		}

		catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

    @GetMapping("/employee/{id}")
	public ResponseEntity<List<Employee>> GetEmployeeById(@PathVariable("id") Long id) {
		try {
			Optional<Employee> employee = this.employeeRepo.findById(id);
			if (employee.isPresent()) {
				ResponseEntity rest = new ResponseEntity<>(employee, HttpStatus.OK);
				return rest;
			}
			else {
				return ResponseEntity.notFound().build();
			}

			
		}

		catch (Exception exception) {
			return new ResponseEntity<>( HttpStatus.NO_CONTENT);
		}
	}
	
//	Edit
	@PutMapping("/employee/{id}")
	public ResponseEntity<Object> UpdateEmployee(@RequestBody Employee employee, @PathVariable("id") Long id) 
	{
		Optional<Employee> employeeData = this.employeeRepo.findById(id);
		if (employeeData.isPresent()) {
			employee.setModifiedBy("Miku");
			employee.setModifiedAt(new Date());
			employee.setId(id);
			this.employeeRepo.save(employee);
			ResponseEntity rest = new ResponseEntity<>("Success", HttpStatus.OK);
			return rest;
		}
		else {
			return ResponseEntity.notFound().build();
		}
	}

    @DeleteMapping("/employee/{id}")
	public ResponseEntity<Object> DeleteEmployee(@PathVariable("id") Long id) 
	{
		Optional<Employee> employeeData = this.employeeRepo.findById(id);
		if (employeeData.isPresent()) {
			this.employeeRepo.deleteById(id);
			ResponseEntity rest = new ResponseEntity<>("Success", HttpStatus.OK);
			return rest;
		}
		else {
			return ResponseEntity.notFound().build();
		}
	}
}
