package com.TugasCRUD.Controller;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.TugasCRUD.Model.Employee_position;
import com.TugasCRUD.Repository.Employee_positionRepo;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiEmployeePosition {
    @Autowired
    private Employee_positionRepo employee_positionRepo;
    
    @GetMapping("/employeepositionbyemployee/{id}")
	public ResponseEntity<List<Employee_position>> GetAllEmployeepositionByEmployeeId(@PathVariable("id") Long id) {
		try {
			List<Employee_position> employee_position = this.employee_positionRepo.FindByEmployeeId(id);
			return new ResponseEntity<>(employee_position, HttpStatus.OK);
		}

		catch (Exception exception) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

    @GetMapping("/employeeposition")
	public ResponseEntity<List<Employee_position>> GetAllEmployeePosition() {
		try {
			List<Employee_position> employee_position = this.employee_positionRepo.findAll();
			return new ResponseEntity<>(employee_position, HttpStatus.OK);
		} catch (Exception exception) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

    @PostMapping("/employeeposition")
	public ResponseEntity<Object> SaveEmployee(@RequestBody Employee_position employee_position) {
		try {
			employee_position.setCreatedBy("Bhaskoro");
			employee_position.setCreatedAt(new Date());
			this.employee_positionRepo.save(employee_position);
			return new ResponseEntity<>("Success", HttpStatus.OK);
		} catch (Exception exception) {
			return new ResponseEntity<>("Failed", HttpStatus.BAD_REQUEST);
		}
	}

    @GetMapping("/employeepositionmapped")
	public ResponseEntity<Map<String, Object>> GetAllPage(@RequestParam(defaultValue = "0") int page,
			@RequestParam(defaultValue = "5") int size) {
		try {
			List<Employee_position> employee_position = new ArrayList<>();
			Pageable pagingSort = PageRequest.of(page, size);

			Page<Employee_position> pageTuts;

			pageTuts = employee_positionRepo.findAll(pagingSort);

			employee_position = pageTuts.getContent();

			Map<String, Object> response = new HashMap<>();
			response.put("employee_position", employee_position);
			response.put("currentPage", pageTuts.getNumber());
			response.put("totalItems", pageTuts.getTotalElements());
			response.put("totalPages", pageTuts.getTotalPages());

			return new ResponseEntity<>(response, HttpStatus.OK);
		}

		catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

    @GetMapping("/employeeposition/{id}")
	public ResponseEntity<Object> GetEmployeePositionById(@PathVariable("id") Long id) {
		try {
			Optional<Employee_position> employee_position = this.employee_positionRepo.findById(id);
			if (employee_position.isPresent()) {
				ResponseEntity rest = new ResponseEntity<>(employee_position, HttpStatus.OK);
				return rest;
			} else {
				return ResponseEntity.notFound().build();
			}

		}

		catch (Exception exception) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

    @PutMapping("/employeeposition/{id}")
	public ResponseEntity<Object> UpdateEmployeePosition(@RequestBody Employee_position employee_position, @PathVariable("id") Long id) {
		Optional<Employee_position> employeepositionData = this.employee_positionRepo.findById(id);
		if (employeepositionData.isPresent()) {
			employee_position.setModifiedBy("Miku");
			employee_position.setModifiedAt(new Date());
			employee_position.setId(id);
			this.employee_positionRepo.save(employee_position);
			ResponseEntity rest = new ResponseEntity<>("Success", HttpStatus.OK);
			return rest;
		} else {
			return ResponseEntity.notFound().build();
		}
	}

	@DeleteMapping("/employeeposition/{id}")
	public ResponseEntity<Object> DeleteEmployeePosition(@PathVariable("id") Long id) {
		Optional<Employee_position> employeepositionData = this.employee_positionRepo.findById(id);
		if (employeepositionData.isPresent()) {
			this.employee_positionRepo.deleteById(id);
			ResponseEntity rest = new ResponseEntity<>("Success", HttpStatus.OK);
			return rest;
		} else {
			return ResponseEntity.notFound().build();
		}
	}
}
