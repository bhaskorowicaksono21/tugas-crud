package com.TugasCRUD.Controller;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.TugasCRUD.Model.Position;
import com.TugasCRUD.Repository.PositionRepo;



@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiPosition {
    @Autowired
    private PositionRepo positionRepo;

    @GetMapping("/position")
    public ResponseEntity<List<Position>> GetAllPosition() {
        try {
            List<Position> position = this.positionRepo.findAll();
            return new ResponseEntity<>(position, HttpStatus.OK);
        } catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }
}
