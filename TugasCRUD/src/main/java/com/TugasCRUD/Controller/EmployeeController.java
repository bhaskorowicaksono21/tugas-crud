package com.TugasCRUD.Controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value = "/employee/")
public class EmployeeController {

    @GetMapping(value = "index")
	public ModelAndView index() {
		ModelAndView view = new ModelAndView("employee/index");
		return view;
	}
}
