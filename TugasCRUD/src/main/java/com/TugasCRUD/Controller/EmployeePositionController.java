package com.TugasCRUD.Controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value = "/employee_position/")
public class EmployeePositionController {
    
    @GetMapping(value = "index")
	public ModelAndView index() {
		ModelAndView view = new ModelAndView("employee_position/index");
		return view;
	}
}
