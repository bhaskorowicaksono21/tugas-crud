package com.TugasCRUD.Model;

import javax.persistence.*;

@Entity
@Table(name = "biodata")
public class Biodata {
    @Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	@Column(name = "id",nullable = false)
	private long id;

    @Column(name = "first_name", nullable = false)
	private String firstName;

    @Column(name = "last_name", nullable = false)
	private String lastName;

    @Column(name = "dob", nullable = false)
	private String Dob;

    @Column(name = "pob", nullable = false)
	private String Pob;

    @Column(name = "address", nullable = false)
	private String Address;

    @Column(name = "marital_status", nullable = false)
	private Boolean maritalStatus;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDob() {
        return Dob;
    }

    public void setDob(String dob) {
        Dob = dob;
    }

    public String getPob() {
        return Pob;
    }

    public void setPob(String pob) {
        Pob = pob;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public Boolean getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(Boolean maritalStatus) {
        this.maritalStatus = maritalStatus;
    }
    
}
