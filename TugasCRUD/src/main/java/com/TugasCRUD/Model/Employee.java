package com.TugasCRUD.Model;

import javax.persistence.*;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;


@Entity
@Table(name = "employee")
// SoftDelete
@SQLDelete(sql = "UPDATE employee SET is_delete = true WHERE id=?")
@Where(clause = "is_delete = false")
public class Employee extends CommonEntity{
    @Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	@Column(name = "id",nullable = false)
	private long id;

    @OneToOne
    @JoinColumn(name = "biodata_id", insertable = false, updatable = false)
    public Biodata biodata;

    @Column(name = "biodata_id", nullable = true)
    private Long BiodataId;

    @Column(name = "nip", nullable = false)
	private String Nip;

    @Column(name = "status", nullable = false)
	private String Status;

    @Column(name = "salary", nullable = false)
	private Long Salary;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Biodata getBiodata() {
        return biodata;
    }

    public void setBiodata(Biodata biodata) {
        this.biodata = biodata;
    }

    public Long getBiodataId() {
        return BiodataId;
    }

    public void setBiodataId(Long biodataId) {
        BiodataId = biodataId;
    }

    public String getNip() {
        return Nip;
    }

    public void setNip(String nip) {
        Nip = nip;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public Long getSalary() {
        return Salary;
    }

    public void setSalary(Long salary) {
        Salary = salary;
    }

    
}
