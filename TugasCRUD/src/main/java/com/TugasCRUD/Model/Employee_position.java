package com.TugasCRUD.Model;

import javax.persistence.*;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

@Entity
@Table(name = "employee_position")
// SoftDelete
@SQLDelete(sql = "UPDATE employee_position SET is_delete = true WHERE id=?")
@Where(clause = "is_delete = false")
public class Employee_position extends CommonEntity{
    @Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	@Column(name = "id",nullable = false)
	private long id;

    @ManyToOne
    @JoinColumn(name = "employee_id", insertable = false, updatable = false)
    public Employee employee;

    @Column(name = "employee_id", nullable = true)
    private Long EmployeeId;

    @ManyToOne
    @JoinColumn(name = "position_id", insertable = false, updatable = false)
    public Position position;

    @Column(name = "position_id", nullable = true)
    private Long PositionId;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Long getEmployeeId() {
        return EmployeeId;
    }

    public void setEmployeeId(Long employeeId) {
        EmployeeId = employeeId;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public Long getPositionId() {
        return PositionId;
    }

    public void setPositionId(Long positionId) {
        PositionId = positionId;
    }

    
}
