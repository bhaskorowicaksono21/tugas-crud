package com.TugasCRUD.Model;

import javax.persistence.*;

@Entity
@Table(name = "position")
public class Position {
    @Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	@Column(name = "id",nullable = false)
	private long id;

    @Column(name = "name", nullable = false)
	private String Name;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    
}
