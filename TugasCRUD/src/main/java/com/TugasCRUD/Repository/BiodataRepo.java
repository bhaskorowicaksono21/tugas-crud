package com.TugasCRUD.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.TugasCRUD.Model.Biodata;

public interface BiodataRepo  extends JpaRepository<Biodata, Long>{
    
}
