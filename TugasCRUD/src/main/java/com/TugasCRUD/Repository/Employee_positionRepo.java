package com.TugasCRUD.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.TugasCRUD.Model.Employee_position;

public interface Employee_positionRepo extends JpaRepository<Employee_position, Long>{
    @Query("FROM Employee_position WHERE EmployeeId = ?1")
    
    List<Employee_position> FindByEmployeeId(Long EmployeeId);

    @Query("FROM Employee_position WHERE PositionId = ?1")
    List<Employee_position> FindByPositionId(Long PositionId);
}
