package com.TugasCRUD.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.TugasCRUD.Model.Position;

public interface PositionRepo extends JpaRepository<Position,Long>{
    
}
