package com.TugasCRUD;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TugasCrudApplication {

	public static void main(String[] args) {
		SpringApplication.run(TugasCrudApplication.class, args);
	}

}
